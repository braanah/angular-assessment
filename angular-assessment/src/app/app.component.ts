import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})


export class AppComponent {
  title = 'angular-assessment';
  stars: string[] = ["people", "planets", "starships", "vehicles", "films", "species"];
  sliceStart: number;
  sliceEnd: number;
  selectedStar: string;
  result: string;

  constructor(private httpClient: HttpClient) {
  }

  starSearch(star: string, id: number) {
    // this.log.log('FilmService', `returning a collection of films`)
    return this.httpClient.get(`https://swapi.co/api/${star}/${id}`)
    // .subscribe(response =>  {
    //   this.result = response;
    // });
  }
  setResArr(star: string, id: number) {
    this.starSearch(star, id).subscribe(response => {
      console.log(response);
      if (response.hasOwnProperty("name")) {
        this.result = response["name"]
      }
      else {
        this.result = response["title"];
      }
    });



  }
}

